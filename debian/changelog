hocr (0.10.18-3) unstable; urgency=medium

  * Update maintainer address (Closes: #899535)
  * Fix lintian useless-autoreconf-build-depends
  * Fix lintian pkg-config-bad-directive

 -- Lior Kaplan <kaplan@debian.org>  Mon, 30 Jul 2018 10:51:22 +0300

hocr (0.10.18-2) unstable; urgency=medium

  * libhocrgtk_ldflags.patch: fix link order. Could fail randomly.
  * libhocrgtk was not removed. Revert symbols file to its state before
    version upgrade.

 -- Tzafrir Cohen <tzafrir@debian.org>  Tue, 24 Jan 2017 21:43:50 +0200

hocr (0.10.18-1) unstable; urgency=medium

  * New upstream release:
    - missing-includes.patch dropped (merged upstream)
    - no-scanner.patch dropped (merged upstream)
    - no-scanner.patch dropped (merged upstream)
    - manpage-whatis.patch dropped (merged upstream)
    - sane-pygtk-desktop-icon.patch dropped (merged upstream)
    - libhocrgtk was removed
    - license is now GPL-3+
  * Remove Baruch Even from Uploaders (Closes: #760010, #760017)
  * Compat level 10 (Closes: #805943)
  * reproducible-build.patch (Closes: #831647)
  * A DEP-5 copyright file
  * Add a symbols file for libhocr0
  * lm.patch: link libhocr with libm
  * spelling.patch: fix some typos reported by Lintian
  * Extra hardening build flags
  * Cannonical VCS URLs. A secure browser URL
  * pkgconfig.patch: fix usage of @pkgincludedir@ in pkg-config file
  * desktop-keywords.patch: add 'Keywords' to desktop files
  * Remove menu entry. Keep existing desktop entry
  * Standards-Version 3.9.8

 -- Tzafrir Cohen <tzafrir@debian.org>  Tue, 24 Jan 2017 13:02:34 +0200

hocr (0.10.17-2) unstable; urgency=low

  [ Lior Kaplan ]
  * python-glade2 dependency missing for hocr-gtk (LP: #981802)
  * Update build dependency from libtiff4-dev to libtiff5-dev

  [ Daniel Schepler ]
  * Update libtool at build time using dh-autoreconf, in order to fix a build 
    failure on x32 (Closes: #702058)

 -- Lior Kaplan <kaplan@debian.org>  Mon, 24 Jun 2013 01:43:41 +0300

hocr (0.10.17-1) unstable; urgency=low

  * The DebConf11 upload
  * New upstream release (LP: #199951)
    - Doesn't support the perl bindings. Dropping libhocr-perl.
  * Switch to dpkg-source 3.0 (quilt) format
  * Migrate from dh_pycentral to dh_python2 (Closes: #616843)

 -- Lior Kaplan <kaplan@debian.org>  Tue, 26 Jul 2011 23:26:49 +0200

hocr (0.8.2-7) unstable; urgency=low

  * Update Standards-Version to 3.9.1
    - Dropped .la from -dev package per policy 10.2 (Closes: #620764)
  * Fix Lintian's errors and warnings (Closes: #476315)

 -- Lior Kaplan <kaplan@debian.org>  Tue, 05 Apr 2011 01:17:10 +0300

hocr (0.8.2-6.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add ${shlib:Depends} to debian/control for libhocr-perl and libhocr-python
    to fix the missing library depends (e.g. on libc)
    (Closes: #553214, #553215)

 -- Patrick Schoenfeld <schoenfeld@debian.org>  Fri, 22 Jan 2010 17:06:50 +0100

hocr (0.8.2-6) unstable; urgency=low

  * Move Homepage from description to control field
  * Add Vcs-* fields
  * Do not ignore errors of make distclean
  * Update Standards-Version to 3.7.3, no changes needed

 -- Baruch Even <baruch@debian.org>  Sat, 29 Dec 2007 17:52:56 +0200

hocr (0.8.2-5) unstable; urgency=low

  * Change menu according to the new menu policy, s/Apps/Applications/

 -- Baruch Even <baruch@debian.org>  Thu, 05 Jul 2007 08:59:31 +0300

hocr (0.8.2-4) unstable; urgency=low

  * Update debian/control so packages are binNMU safe.
    See http://wiki.debian.org/binNMU for more info.

 -- Lior Kaplan <kaplan@debian.org>  Fri, 22 Jun 2007 11:28:17 +0100

hocr (0.8.2-3) unstable; urgency=low

  [ Lior Kaplan ]
  * Fix typo in libhocr-perl description (Closes: #405646)

  [ Baruch Even ]
  * Upgrade dependency on python-central to >= 0.5 (Closes: #427281)

 -- Baruch Even <baruch@debian.org>  Sun, 03 Jun 2007 02:28:13 +0300

hocr (0.8.2-2) unstable; urgency=low

  * Comply with the new Python policy.

 -- Lior Kaplan <kaplan@debian.org>  Wed,  5 Jul 2006 20:51:32 +0300

hocr (0.8.2-1) unstable; urgency=low

  * New upstream release
    - Adds Swedish translation to hocr-gtk (Closes: #349735)
  * debian/control: upgrade to standards version to 3.7.2.0 (no changes needed)
  * debian/menu: add menu icon to hocr-gtk

 -- Lior Kaplan <kaplan@debian.org>  Sun, 21 May 2006 01:02:51 -0500

hocr (0.8-1) unstable; urgency=low

  * New upstream release

 -- Baruch Even <baruch@debian.org>  Sun, 15 Jan 2006 21:19:01 +0000

hocr (0.7.1-2) unstable; urgency=low

  * Add bindings for Python & Perl.
  * Add hocr-gtk to the Debian menu.

 -- Lior Kaplan <webmaster@guides.co.il>  Sat,  7 Jan 2006 11:53:40 +0200

hocr (0.7.1-1) unstable; urgency=low

  * New upstream release

 -- Lior Kaplan <webmaster@guides.co.il>  Tue,  3 Jan 2006 21:11:03 +0200

hocr (0.4.6-1) unstable; urgency=low

  * Initial release. Closes: #325342

 -- Lior Kaplan <webmaster@guides.co.il>  Fri,  2 Dec 2005 15:30:45 +0200

