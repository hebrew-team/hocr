��    =        S   �      8     9     I     a  	   {     �     �     �     �  J   �     �               )     0     C     O     [     b     k     y     �     �     �  $   �  	   �     �     �  !   �                              '     0     5     A     E     M     S     [     b     g     p     x     }     �     �     �  
   �     �     �     �     �     �     �  	   �  
   �     �  
   �  \       _	  $   q	  $   �	  
   �	     �	     �	     �	     �	  V   
  
   m
     x
     �
  
   �
     �
     �
     �
     �
  
   �
       
     
        (     1  ,   F     s          �  &   �  
   �     �     �  
   �     �               '     C     L     U  
   \     g     t  
   {  
   �     �  
   �     �  
   �     �     �  
   �     �     �  
   �     �               %     7     K     2               1      
          =       -      )          :                      6   	   7                       +       %             $          ,   4   <           "   5   0      *          8             #   3             &      (                          ;      !      .      /   9      '        Black and White Closed, no files scaned Closed, no files selected Direction Engine Font Font Recognition... Font splicing Hocr-GTK, Hebrew optical character recognition
graphical front end (GTK)

 Image Image Processing... Image processing Layout Layout Analysis... Line leeway Linguistics Open.. Original Processing... Save.. Scale Scan Set text font Shlomi Israel <sijproject@gmail.com> Threshold Threshold type Word spacing Yaacov Zamir <kzamir@walla.co.il> _Edit _File _Help _View adaptive agresive auto auto rotate big careful clear closing column dark dilation erosion fine free light nikud normal one column opening regular small spaced tight translator-credits very dark very light very spaced very tight Project-Id-Version: libhocr 0.10.9
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-04 11:36+0200
PO-Revision-Date: 2008-05-11 08:22+0300
Last-Translator: kobi <kzamir@walla.co.il>
Language-Team: Hebrew
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 שחור ולבן סוגר. לא נסרקו קבצים סוגר. לא נבחרו קבצים כיוון מנוע סוג אות זיהוי תווים ... חיתוך בין אותיות Hocr-GTK, מזהה תווים אופטי עברי
מנשק משתמש גרפי (GTK)

 תמונה עיבוד תמונה ... עיבוד תמונה עימוד זיהוי עימוד ... רווח בין שורות לשון פתח.. מקורי מעבד ... שמור.. הגדלה סרוק קבע סוג אות שלומי ישראל <sijproject@gmail.com> רמת סף סוג רמת סף ריווח בין מילים יעקב זמיר <kzamir@walla.co.il> עריכה קובץ עזרה תצוגה גמיש אגרסיבי אוטומאטי סיבוב אוטומאטי גדול זהיר נקה סגירה עמודות כהה הרחבה שחיקה עדין חופשי בהיר ניקוד רגיל עמודה אחת פתיחה רגיל קטן מרווח צפוף יעקב זמיר מאוד כהה מאוד בהיר מאוד מרווח מאוד צפוף 